from lrs.forms import SignUpForm
from rest_framework import status
from django.shortcuts import render
from rest_framework import viewsets
from django.views.generic import View
from configurations.models import (MyUser)
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout


class LoginView(View):

	""" Login User by username and passowrd """

	def get(self, request):
		if request.user.id is not None:
			return redirect("dashboard",)
		return render(request, 'login.html')


	def post(self, request):
		try:
			user = MyUser.objects.get(username=request.POST.get('username'),user_type="Laboratory")
			# if user.check_password(request.POST['password']):
			user = authenticate(username=request.POST.get('username'),
						password=str(request.POST.get('password')))
			if user is not None:
				login(request, user)
				return redirect('dashboard')
			return render(request, 'login.html',
				{'invalidUserPsd': "Please enter a valid username or password"})
		except MyUser.DoesNotExist:
			return render(request, 'login.html',
				{'errorMessage': "Please, SignUp. Link is given below."})


class SignUpView(View):

	""" SignUp User by username and passowrd """

	def get(self, request):
		return render(request, 'signup.html')

	def post(self, request):
		password = str(request.POST['currentpassword'])
		if str(request.POST.get('currentpassword')) != str(request.POST.get('confirmpassowrd')):
			return render(request,'signup.html',{'errorMessage': "Passwords dosen't match !!!"})
		try:
			user = MyUser.objects.get(username=request.POST.get('username'))
			if user is not None:
				return render(request,'signup.html',{'existMessage': "This username is already exist !!!"})
		except MyUser.DoesNotExist:		
			sign_form = SignUpForm(request.POST)
			if sign_form.is_valid():
				user = sign_form.save(commit=False)
				user.user_type = "Laboratory"
				user.set_password(password)
				user.save()
				return redirect('login')
			return render(request,'signup.html',{'sign_form':sign_form})


@method_decorator(login_required, name='dispatch')
class LogoutView(View):
	""" Logout """
	def get(self, request):
		logout(request)
		return redirect('login')


@method_decorator(login_required, name='dispatch')
class DashboardView(View):

	""" Dashboard """

	def get(self, request):
		return render(request, "dashboard.html")