from django import forms
from configurations.models import (MyUser)



class SignUpForm(forms.ModelForm):

    class Meta:
        model = MyUser
        exclude = ('password',)
        # fields = "__all__"