from django.contrib import admin
from django.urls import path
from lrs.views import *


urlpatterns = [

	path('',LoginView.as_view(), name="login"),

	path('register',SignUpView.as_view(), name="register"),

	path('logout',LogoutView.as_view(), name="logout"),

	path('dashboard',DashboardView.as_view(), name="dashboard"),

]