from django.db import models
from configurations.models import (MyUser,Trackable)
# Create your models here.
ORGANIZATION_CHOICES = (
    ('1','Government'),
    ('2','Private'),
    ('3','Autonomous'),
    ('4','Public Sector'),
    ('5','Ltd. Co.'),
)

DOCUMENT_TYPE = (
    ('1','LabAddress'),
    ('2','OrganisationProof'),
    ('3','Statuary ComplienceProof'),
)

DOCUMENT_TYPE = (
    ('1','LabAddress'),
    ('2','OrganisationProof'),
    ('3','Statuary ComplienceProof'),
)


class Top_management(Trackable):
    user = models.ForeignKey(MyUser,verbose_name='Lab User', on_delete=models.CASCADE)
    top_management_name = models.CharField(max_length=50, blank=True, null=True)
    top_management_designation = models.CharField(max_length=50, blank=True, null=True)
    top_management_mobile_dialcode = models.CharField(max_length=10, blank=True, null=True)
    top_management_mobile = models.CharField(max_length=10, null=True, blank=True)
    top_management_telephone_dialcode = models.CharField(max_length=10, null=True, blank=True)
    top_management_telephone_areacode = models.CharField(max_length=10, null=True, blank=True)
    top_management_telephone = models.CharField(max_length=10, null=True, blank=True)
    top_management_fax = models.CharField(max_length=10, blank=True, null=True)


class Laboratory(Trackable):
    user = models.ForeignKey(MyUser, db_index=True, related_name="user_laboratory",
                                verbose_name='User', on_delete=models.CASCADE)
    lab_id_no = models.CharField(max_length=100,blank=True, null=True, verbose_name='Laboratory id No')
    lab_name = models.CharField(max_length=100, verbose_name='Laboratory Name')
    lab_address = models.TextField(blank=True, null=True)