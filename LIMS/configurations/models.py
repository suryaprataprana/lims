import uuid

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.core.validators import MaxValueValidator
from django.utils.timezone import now
from datetime import datetime

class MyUserManager(BaseUserManager):
    def create_user(self, username, password=None, **extra_fields):
        
        if not username:
            raise ValueError('Users must have an username')
        account = self.model(
            username = username.lower()
        )
        
        account.account_type = extra_fields.get('account_type')
        account.set_password(password)
        account.save(using=self._db)
        return account

    def create_superuser(self, username, password, **extra_fields):
        
        account = self.create_user(
            username=username,
            password=password,
        )
        account.user_type = 'Django Admin'
        account.is_admin = True
        account.save(using=self._db)
        return account


class MyUser(AbstractBaseUser):
    user_choice = (('Django Admin', 'Django Admin'),
                   ('Admin','Admin'),
                   ('Laboratory','Laboratory'),
                   ('Auditor','Auditor'),
                   )

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
    )
    mobile = models.CharField(max_length=15,null=True,blank=True)
    name = models.CharField(max_length=100,null=True,)
    username = models.CharField(max_length=255,unique=True)
    user_type = models.CharField(choices=user_choice, max_length=30, null=True,blank=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    created_at = models.DateTimeField("Created Date", auto_now_add=True)
    updated_at = models.DateTimeField("Updated Date", auto_now=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = 'MyUser'
        verbose_name_plural = 'MyUser'

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
         return True

    @property
    def is_staff(self):
        return self.is_admin

class Trackable(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(MyUser, related_name='created_%(class)s', on_delete=models.CASCADE, blank=True, null=True)
    updated_by = models.ForeignKey(MyUser, related_name='updated_%(class)s', on_delete=models.CASCADE, blank=True, null=True)
    class Meta:
        abstract = True



class Country(Trackable):
    name = models.CharField(max_length=100)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'

    def __str__(self):
        return str(self.country)


class State(Trackable):
    state=models.CharField(max_length=50)
    country=models.ForeignKey(Country, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    class Meta:
        unique_together = ('country','state')
        verbose_name = 'State'
        verbose_name_plural = 'States'
    def __str__(self):
        return str(self.state)


class City(Trackable):
    city=models.CharField(max_length=50)
    state=models.ForeignKey(State, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    class Meta:
        unique_together = ('state', 'city')
        verbose_name = 'State'
        verbose_name_plural = 'States'
    def __str__(self):
        return str(self.state)

